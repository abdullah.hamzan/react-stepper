import React, { useState } from 'react'

import {Stepper,Step,StepLabel,Button} from "@material-ui/core";
import { makeStyles } from '@material-ui/styles';

const useStyles=makeStyles({
  buttonGroup:{
    display:"flex",
    flexDirection:"row",
    justifyContent:"center",
    alignContent:"center"

  }
})

const App = () => {
  const classes=useStyles();

  const [activeStep,setActipStep]=useState(0);


  const handleClick=(params)=>{

    setActipStep(params+1)

  }

  const handleReset=(params)=>{

    if (params>0){

      setActipStep(0);

    }


  }

  const handleBack=(params)=>{
    
    if (params>0){
      setActipStep(params-=1)
    }

  }
  

  console.warn("activeStep:",activeStep)
  return (
    <div>
      <Stepper  activeStep={activeStep}>
        <Step>
          <StepLabel>first</StepLabel>
        </Step>
        <Step>
          <StepLabel>Second</StepLabel>
        </Step>
        <Step>
          <StepLabel>Third</StepLabel>
        </Step>

      </Stepper>

      <div className={classes.buttonGroup}>
      <Button style={{
        marginRight:"10px"
      }} variant="contained" color="primary" onClick={()=>handleClick(activeStep)} >
        next step</Button>
        <Button style={{
        marginRight:"10px"
      }} variant="contained" color="primary" onClick={()=>handleBack(activeStep)} >
        before step</Button>
      <Button variant='contained' color="secondary" onClick={()=>handleReset(activeStep)} >Reset</Button>
      </div>
    
      
      </div>
  )
}

export default App